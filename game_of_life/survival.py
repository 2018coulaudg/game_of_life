from amorce import *

def survival(cellule, universe):
    x, y = cellule
    n, m = universe.shape
    voisines_vivantes = 0
    voisines_mortes = 0
    for i in range(y-1, y+2):
        for j in range(x-1, x+2):
            if i >= 0 and i < n and j >= 0 and j < m and (i,j) != (y, x):
                if universe[i][j]==1:
                    voisines_vivantes+=1
                else:
                    voisines_mortes +=1
    if universe[y][x] == 1:
        if voisines_vivantes == 2 or voisines_vivantes == 3:
            return 1 #cellule vivante
        else:
            return 0 #cellule_morte
    else:
        if voisines_vivantes == 3:
            return 1
        else:
            return 0

def generation(universe):
    n, m = universe.shape
    new_universe = generate_universe((n, m))
    for i in range(n):
        for j in range(m):
            new_universe[i][j] = survival((j, i), universe)
    return new_universe




