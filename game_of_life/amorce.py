from generate_universe import *
from catalogue import *

def amorce(seed, universe):
    s = seeds[seed]
    (i, j) = (len(s), len(s[0]))
    (n, m) = universe.shape
    assert n >= i and m >= j
    return add_seed_to_universe(s, universe)
