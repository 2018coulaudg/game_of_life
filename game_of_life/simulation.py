from survival import *
import matplotlib.animation as anim


def game_life_simulation(universe, p):  # p est le nombre d'itérations
    for i in range(1, p):
        universe = generation(universe)
    return universe


def simulation_anime(universe_size, seed, seed_init_position, cmap, n, time_interval):
    fig = plt.figure()
    universe = add_seed_to_universe(seed, generate_universe(universe_size), seed_init_position)
    im = plt.imshow(universe, cmap = cmap)
    def anime(i):
        nonlocal universe
        universe = generation(universe)
        im.set_array(universe)
        return im,
    global animation
    animation = anim.FuncAnimation(fig, anime, frames = n, interval = time_interval, blit = True, repeat = False)
    plt.show()


